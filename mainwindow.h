#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  explicit MainWindow(QWidget *parent = nullptr);
  bool event(QEvent *ev) override;

signals:

private:
  void *centralWidget;
  void *vBox;
  void *textEdit;
  void *quitButton;
  void buildUi();
  static QString saveFileName;
  bool textChanged;

private slots:
  void updateTextChanged();

protected:
  void closeEvent(QCloseEvent *ce);
};

#endif // MAINWINDOW_H
