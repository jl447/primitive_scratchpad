#include <QDebug>
#include <QFile>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

#include "mainwindow.h"

QString MainWindow::saveFileName {
  qEnvironmentVariable("HOME") + "/.local/notes.txt"
};

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow{parent}, textChanged{false}
{
  buildUi();
  QObject::connect(static_cast<QPushButton *>(quitButton),
                   &QPushButton::clicked,
                   this, &QMainWindow::close);
  QObject::connect((QPlainTextEdit *)textEdit, &QPlainTextEdit::textChanged,
                   this, &MainWindow::updateTextChanged);
}

void
MainWindow::buildUi()
{
  centralWidget = new QWidget { this };
  vBox = new QVBoxLayout;
  textEdit = new QPlainTextEdit;
  quitButton = new QPushButton { "Quit" };
  ((QVBoxLayout *)vBox)->addWidget((QWidget *)textEdit);
  ((QVBoxLayout *)vBox)->addWidget((QWidget *)quitButton);
  ((QWidget *)centralWidget)->setLayout((QVBoxLayout *)vBox);
  setCentralWidget((QWidget *)centralWidget);
  QFile f { MainWindow::saveFileName };
  if (f.open(QIODevice::ReadOnly)) {
    ((QPlainTextEdit *)textEdit)->appendPlainText(f.readAll());
    f.close();
  }
}

void
MainWindow::updateTextChanged()
{
  if (!textChanged) {
    textChanged = true;
  }
}

void
MainWindow::closeEvent(QCloseEvent *ce)
{
  if (textChanged) {
    QFile f { MainWindow::saveFileName };
    if (f.open(QIODevice::WriteOnly)) {
      f.write(((QPlainTextEdit *)textEdit)->toPlainText().toLocal8Bit());
      f.close();
    }
  } else {
    qDebug() << "No updates to text, not saving";
  }
  ce->accept();
}

bool
MainWindow::event(QEvent *ev)
{
  if (ev->type() == QEvent::KeyPress) {
    QKeyEvent *k = (QKeyEvent *)ev;
    if (k->key() == Qt::Key_Escape) {
      close();
    }
    return true;
  }
  return QWidget::event(ev);
}
